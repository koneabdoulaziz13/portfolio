<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=array(
            'description'=>"Prét à relever les défis tout en avançant sereinement.",
            'short_des'=>"Ingénieur en Sciences Informatiques.",
            'photo'=>"image.jpg",
            'logo'=>'logo.jpg',
            'address'=>"Abidjan- Cocody, Saint-Jean ",
            'email'=>"koneabdoulaziz13@gmail.com",
            'phone'=>"+225 07 78 37 66 39 / +225 05 05 66 42 43",
        );
        DB::table('settings')->insert($data);
    }
}
