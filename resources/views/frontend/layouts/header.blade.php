<header class="header shop">
    <!-- Topbar -->
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-12">
                    <!-- Top Left -->
                    <div class="top-left">
                        <ul class="list-main">
                            @php
                            $settings=DB::table('settings')->get();

                            @endphp
                            <li><i class="ti-headphone-alt"></i>@foreach($settings as $data) {{$data->phone}} @endforeach</li>
                            <li><i class="ti-email"></i> @foreach($settings as $data) {{$data->email}} @endforeach</li>
                        </ul>
                    </div>
                    <!--/ End Top Left -->
                </div>
                <div class="col-lg-6 col-md-12 col-12">
                    <!-- Top Right -->
                    <div class="right-content">
                        <ul class="list-main">
                            <li><i class="ti-location-pin"></i> <a href="{{route('contact')}}">Abidjan Cocody, Saint-Jean </a></li>
                            {{-- <li><i class="ti-alarm-clock"></i> <a href="#">Daily deal</a></li> --}}
                            @auth
                            @if(Auth::user()->role=='admin')
                            <li><i class="ti-user"></i> <a href="{{route('admin')}}" target="_blank">Tableau de bord</a></li>
                            @else
                            <li><i class="ti-user"></i> <a href="{{route('user')}}" target="_blank">Tableau de bord</a></li>
                            @endif
                            <li><i class="ti-power-off"></i> <a href="{{route('user.logout')}}">Deconnection</a></li>

                            @else
                            <li><i class="ti-power-off"></i><a href="{{route('login.form')}}">Se connecter /</a> <a href="{{route('register.form')}}">S'enregistrer</a></li>
                            @endauth
                        </ul>
                    </div>
                    <!-- End Top Right -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Topbar -->



    <!-- Header Inner -->
    <div class="header-inner">
        <div class="col">

        </div>
        <div class="col">

            <div class="container">
                <div class="cat-nav-head">
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="menu-area">
                                <!-- Main Menu -->
                                <nav class="navbar navbar-expand-lg">
                                    <div class="navbar-collapse">
                                        <div class="nav-inner">
                                            <ul class="nav main-menu menu navbar-nav">
                                                <li class="{{Request::path()=='home' ? 'active' : ''}}"><a href="{{route('home')}}">Accueil</a></li>
                                                <li class="{{Request::path()=='about-us' ? 'active' : ''}}"><a href="{{route('about-us')}}">Apropos de nous</a></li>
                                                <!-- <li class="@if(Request::path()=='product-grids'||Request::path()=='product-lists')  active  @endif"><a href="{{route('product-grids')}}">Products</a><span class="new">New</span></li>	 -->
                                                {{Helper::getHeaderCategory()}}
                                                <li class="{{Request::path()=='blog' ? 'active' : ''}}"><a href="{{route('blog')}}">Blog</a><span class="new">Nouveau</span></li>

                                                <li class="{{Request::path()=='contact' ? 'active' : ''}}"><a href="{{route('contact')}}">Contactez Nous</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <!--/ End Main Menu -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col">

        </div>

    </div>
    <!--/ End Header Inner -->
</header>